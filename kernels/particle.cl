__kernel void particle(__global float4 *particles, int count, int start, int end, int numDivisions)
{
	int workSize = get_local_size(0);	// number of kernel instances in this work group
	float4 pos;							// position of current particle
	float4 vel;							// velocity of current particle
	float4 otherPos;					// position of other particle we're interacting with
	float4 diff;						// difference in the two particles' positions
	float distSq;						// distance squared between the two particles
	float invDistSq;					// 1.0 / distSq
	int i, j;

	// operate only on the particle that we're supposed to
	i = (get_group_id(0) * workSize) + get_local_id(0);

	// retrieve current work group particle position
	__global float4 *posPtr = &particles[i];
	pos = *posPtr;

	// now retrieve its velocity
	__global float4 *velPtr = posPtr + count;
	vel = *velPtr;

	// but, each particle in our work group must interact with all others in the currently active rotating subset
	__global float4 *otherPosPtr = &particles[start];
	for(j = start; j < end; j ++)
	{
		// particles do not interact with themselves
		if(posPtr != otherPosPtr)
		{
			// retrieve other particle position
			otherPos = *otherPosPtr;

			// compute squared distance
			diff = pos - otherPos;
			distSq = (diff.x * diff.x) + (diff.y * diff.y) + (diff.z * diff.z);
			invDistSq = (float)numDivisions / distSq;

			// apply attractive force on current work group particle
			vel -= diff * invDistSq;
		}
		otherPosPtr ++;
	}

	// apply attractive force to center of world to keep everything centered (although even
	// without this attraction the particles will stay together, we want them centered...
	// plus, the extra dynamics make it look really cool)
	vel -= pos * 25.0f;
	(*velPtr) = vel;			// update current particle velocity

	// and now move the particle
	*(posPtr) += vel * 0.000001f;
}
