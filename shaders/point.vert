#version 330

uniform mat4 u_Projection;							// camera projection (e.g., orthographic, perspective)
uniform mat4 u_View;								// position and orientation of camera

layout(location = 0) in vec4 a_Vertex;				// incoming position of vertex

void main()
{
	// matrix transformations are always evaluated in reverse order, so the "vertex" vector
	// multiplication here is done first, which is what we want, and so on...
	gl_Position = u_Projection * u_View * vec4(a_Vertex.x, a_Vertex.y, a_Vertex.z, 1.0);
}
