#version 330

out vec4 f_FragColor;

void main()
{
	// all we need to do is assign the outgoing fragment color to the interpolated vertex color value
	f_FragColor = vec4(0.2, 0.5, 1.0, 0.03);
}
