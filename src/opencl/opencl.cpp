#include "opencl/opencl.h"

#include "cl/cl.h"

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

OpenCL *OpenCL::instance = NULL;

OpenCL::OpenCL()
{
	setup();
}

OpenCL::~OpenCL()
{
	clReleaseContext(context);
	// kernels, programs, and buffers should be released by higher-level code

	// nullify invalid instance pointer
	instance = NULL;

	// free platform and device information
	delete[] deviceIDs;
	delete[] platformIDs;
}

OpenCL *OpenCL::getInstance()
{
	if(instance == NULL)
	{
		instance = new OpenCL();
	}

	return instance;
}

void OpenCL::setup()
{
	cl_int error;

	// determine how many OpenCL platforms we have
	platformIDCount = 0;
	error = clGetPlatformIDs(0, NULL, &platformIDCount);
	checkError("glGetPlatformIDs()", error);
	if(platformIDCount == 0)
	{
		cout << "No platforms found" << endl;
		exit(1);
	}
	else
	{
		cout << "-- Found " << platformIDCount << " OpenCL platform(s)" << endl;
	}

	// get the IDs of those platforms
	platformIDs = new cl_platform_id[platformIDCount];
	error = clGetPlatformIDs(platformIDCount, platformIDs, NULL);
	checkError("clGetPlatformIDs()", error);

	// determine how many OpenCL devices we have
	deviceIDCount = 0;
	error = clGetDeviceIDs(platformIDs[0], CL_DEVICE_TYPE_GPU, 0, NULL, &deviceIDCount);
	checkError("clGetDeviceIDs()", error);
	if(deviceIDCount == 0)
	{
		cout << "No devices found" << endl;
		exit(1);
	}
	else
	{
		cout << "-- Found " << deviceIDCount << " OpenCL device(s)" << endl;
	}

	// get the IDs of those devices
	deviceIDs = new cl_device_id[deviceIDCount];
	error = clGetDeviceIDs(platformIDs[0], CL_DEVICE_TYPE_GPU, deviceIDCount, deviceIDs, NULL);
	checkError("clGetDeviceIDs()", error);

	// we want to create a context on the platform we found
	const cl_context_properties CONTEXT_PROPERTIES[] =
	{
		CL_CONTEXT_PLATFORM,
		reinterpret_cast<cl_context_properties> (platformIDs[0]),
		0, 0
	};

	// create our context using the given platform
	context = clCreateContext(CONTEXT_PROPERTIES,
							  deviceIDCount,
							  deviceIDs,
							  NULL,
							  NULL,
							  &error);
	checkError("clCreateContext()" , error);
}

void OpenCL::createKernel(const string &sourceFile, const string &kernelName, cl_program &program, cl_kernel &kernel)
{
	char *build_log;						// error log if compilation failed
	size_t ret_val_size;					// length of build log in bytes
	cl_build_status build_status;			// build status of our attempted program compilation
	cl_int error = CL_SUCCESS;

	// load up our program source code and attempt to compile it
	createProgram(loadKernel(sourceFile.c_str()), program);
	/*error = */clBuildProgram(program, deviceIDCount, deviceIDs, "", NULL, NULL);
	//checkError("clBuildProgram()", error);

	// did it successfully compile?
	error = clGetProgramBuildInfo(program, deviceIDs[0], CL_PROGRAM_BUILD_STATUS, sizeof(cl_build_status), &build_status, NULL);
	checkError("clGetProgramBuildInfo()", error);

	// if not, show the build log so we get detailed error messages
	if (build_status != CL_SUCCESS)
	{
		// how long the build log is, in bytes
		error = clGetProgramBuildInfo(program, deviceIDs[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &ret_val_size);
		checkError("clGetProgramBuildInfo()", error);

		// now that we have the length, get the actual build log
		build_log = new char[ret_val_size+1];
		error = clGetProgramBuildInfo(program, deviceIDs[0], CL_PROGRAM_BUILD_LOG, ret_val_size, build_log, NULL);
		checkError("clGetProgramBuildInfo()", error);

		// to be careful, terminate with NULL char---there's no information in the reference whether the string is 0 terminated or not
		build_log[ret_val_size] = '\0';
		cout << "BUILD LOG: '" << sourceFile << "'" << endl;
		cout << build_log << endl;

		// free up our memory and quit forcefully
		delete[] build_log;
		exit(1);
	}

	// everything was successful at this point so far, so create our kernel using the compiled program
	kernel = clCreateKernel(program, kernelName.c_str(), &error);
	checkError("clCreateKernel()", error);
}

void OpenCL::createProgram(const std::string &source, cl_program &program)
{
	size_t lengths[1] = {source.size()};
	const char *sources[1] = {source.data()};
	cl_int error = CL_SUCCESS;

	// pass in the program source so we can create our program
	program = clCreateProgramWithSource(context,		// our CL context
										1,				// 1 source string
										sources,		// our source string(s)
										lengths,		// lengths of our source string(s)
										&error);		// any error?
	checkError("clCreateProgramWithSource()", error);
}

string OpenCL::loadKernel(const string &filename)
{
	ifstream in(filename.c_str());
	string result((istreambuf_iterator<char>(in)), istreambuf_iterator<char>());
	return result;
}

void OpenCL::checkError(cl_int error)
{
	if (error != CL_SUCCESS)
	{
		cerr << "OpenCL call failed with error: " << getErrorString(error) << endl;
		exit(1);
	}
}

void OpenCL::checkError(const std::string &description, cl_int error)
{
	if(error != CL_SUCCESS)
	{
		cerr << description << " failed with error: " << getErrorString(error) << endl;
		exit(1);
	}
}

cl_context OpenCL::getContext()
{
	return context;
}

cl_device_id OpenCL::getDeviceID(int index)
{
	return deviceIDs[index];
}

// thanks, StackOverflow!
// https://stackoverflow.com/a/24336429
string OpenCL::getErrorString(cl_int error)
{
	switch(error)
	{
		// run-time and JIT compiler errors
		case 0: return "CL_SUCCESS";
		case -1: return "CL_DEVICE_NOT_FOUND";
		case -2: return "CL_DEVICE_NOT_AVAILABLE";
		case -3: return "CL_COMPILER_NOT_AVAILABLE";
		case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
		case -5: return "CL_OUT_OF_RESOURCES";
		case -6: return "CL_OUT_OF_HOST_MEMORY";
		case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
		case -8: return "CL_MEM_COPY_OVERLAP";
		case -9: return "CL_IMAGE_FORMAT_MISMATCH";
		case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
		case -11: return "CL_BUILD_PROGRAM_FAILURE";
		case -12: return "CL_MAP_FAILURE";
		case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
		case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
		case -15: return "CL_COMPILE_PROGRAM_FAILURE";
		case -16: return "CL_LINKER_NOT_AVAILABLE";
		case -17: return "CL_LINK_PROGRAM_FAILURE";
		case -18: return "CL_DEVICE_PARTITION_FAILED";
		case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

		// compile-time errors
		case -30: return "CL_INVALID_VALUE";
		case -31: return "CL_INVALID_DEVICE_TYPE";
		case -32: return "CL_INVALID_PLATFORM";
		case -33: return "CL_INVALID_DEVICE";
		case -34: return "CL_INVALID_CONTEXT";
		case -35: return "CL_INVALID_QUEUE_PROPERTIES";
		case -36: return "CL_INVALID_COMMAND_QUEUE";
		case -37: return "CL_INVALID_HOST_PTR";
		case -38: return "CL_INVALID_MEM_OBJECT";
		case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
		case -40: return "CL_INVALID_IMAGE_SIZE";
		case -41: return "CL_INVALID_SAMPLER";
		case -42: return "CL_INVALID_BINARY";
		case -43: return "CL_INVALID_BUILD_OPTIONS";
		case -44: return "CL_INVALID_PROGRAM";
		case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
		case -46: return "CL_INVALID_KERNEL_NAME";
		case -47: return "CL_INVALID_KERNEL_DEFINITION";
		case -48: return "CL_INVALID_KERNEL";
		case -49: return "CL_INVALID_ARG_INDEX";
		case -50: return "CL_INVALID_ARG_VALUE";
		case -51: return "CL_INVALID_ARG_SIZE";
		case -52: return "CL_INVALID_KERNEL_ARGS";
		case -53: return "CL_INVALID_WORK_DIMENSION";
		case -54: return "CL_INVALID_WORK_GROUP_SIZE";
		case -55: return "CL_INVALID_WORK_ITEM_SIZE";
		case -56: return "CL_INVALID_GLOBAL_OFFSET";
		case -57: return "CL_INVALID_EVENT_WAIT_LIST";
		case -58: return "CL_INVALID_EVENT";
		case -59: return "CL_INVALID_OPERATION";
		case -60: return "CL_INVALID_GL_OBJECT";
		case -61: return "CL_INVALID_BUFFER_SIZE";
		case -62: return "CL_INVALID_MIP_LEVEL";
		case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
		case -64: return "CL_INVALID_PROPERTY";
		case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
		case -66: return "CL_INVALID_COMPILER_OPTIONS";
		case -67: return "CL_INVALID_LINKER_OPTIONS";
		case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

		// extension errors
		case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
		case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
		case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
		case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
		case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
		case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
		default: return "Unknown OpenCL error";
    }
}
