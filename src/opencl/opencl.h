#pragma once

#include "cl/cl.h"

#include <string>

class OpenCL
{
private:
	static OpenCL *instance;				// our singleton instance

	cl_uint platformIDCount;				// how many platforms CL found
	cl_platform_id *platformIDs;			// the IDs of those platformIDCount platforms

	cl_uint deviceIDCount;					// how many devices CL found
	cl_device_id *deviceIDs;				// the IDs of those deviceIDCount devices

    cl_context context;						// our CL context handle

	OpenCL();

	void setup();
	std::string getErrorString(cl_int error);
	std::string loadKernel(const std::string &filename);

	void createProgram(const std::string &source, cl_program &program);

public:
	static OpenCL *getInstance();

	~OpenCL();

	void createKernel(const std::string &sourceFile, const std::string &kernelName, cl_program &program, cl_kernel &kernel);
	void checkError(cl_int error);
	void checkError(const std::string &description, cl_int error);

	cl_context getContext();
	cl_device_id getDeviceID(int index);
};
