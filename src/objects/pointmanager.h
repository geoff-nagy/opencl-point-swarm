#pragma once

#include "CL/cl.h"

#include "GL/glew.h"

#include "glm/glm.hpp"

class Shader;

class PointManager
{
private:
	Shader *shader;						// GL shader program

	GLuint vao;							// GL array object ID
	GLuint vbos[1];						// vertex positions

	glm::vec4 *particles;				// used to initialize our CL memory buffer with particle data
	int numPoints;

	int divisionIndex;					// each particle interacts with a rotating subset of all the other particles,
	int numDivisions;					// since it's too expensive to have every particle interact with all others
										// at every time step

	cl_mem particleBuffer;				// CL memory buffer containing the particle data [pos0,..,posn,vel0,...veln]
	cl_command_queue commandQueue;		// CL command queue for sending instructions to the GPU
	cl_program program;					// CL program we run
	cl_kernel kernel;					// refers to the particle() kernel in particle.cl

	void setupVBOs();
	void setupShader();
	void setupPoints();
	void setupOpenCL();
public:
	PointManager(int numPoints);
	~PointManager();

	void update();
	void render(const glm::mat4 &projection, const glm::mat4 &view);
};
