#include "objects/pointmanager.h"

#include "util/shader.h"

#include "opencl/opencl.h"

#include "GL/glew.h"

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/random.hpp"
using namespace glm;

#include <cstdlib>
#include <vector>
#include <iostream>
using namespace std;

PointManager::PointManager(int numPoints)
{
	this -> numPoints = numPoints;
	particles = new vec4[numPoints * 2];			// * 2 because first half is position, second half is velocity

	divisionIndex = 0;
	numDivisions = 25000;//50000;

	setupVBOs();
	setupShader();
	setupPoints();
	setupOpenCL();
}

PointManager::~PointManager()
{
	glDeleteBuffers(1, vbos);
	glDeleteVertexArrays(1, &vao);
	delete shader;

	clReleaseMemObject(particleBuffer);
	clReleaseCommandQueue(commandQueue);
	clReleaseKernel(kernel);
	clReleaseProgram(program);

	delete OpenCL::getInstance();
}

void PointManager::setupVBOs()
{
	// set up our VAO and VBOs
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, vbos);

	// vertex positions
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * numPoints, NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
}

void PointManager::setupShader()
{
	shader = new Shader("shaders/point.vert", "shaders/point.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> link();
	shader -> bind();
	// uniforms go here
	shader -> unbind();
}

void PointManager::setupPoints()
{
	const float RANDOMIZE_RADIUS = 0.34;//1.0;//1.09;//1.0;
	const float RANDOMIZE_VELOCITY = 0.0;

	int i;
	vec4 *vertexPtr = particles;

	// randomize positions
	for(i = 0; i < numPoints; i ++)
	{
		(*vertexPtr++) = vec4(ballRand(RANDOMIZE_RADIUS), 0.0);
	}

	// randomize velocities
	for(i = 0; i < numPoints; i ++)
	{
		(*vertexPtr++) = vec4(ballRand(RANDOMIZE_VELOCITY), 0.0);
	}
}

void PointManager::setupOpenCL()
{
	OpenCL *cl = OpenCL::getInstance();
	cl_int error;

	// create our kernel program to handle the particle interactions
	cl -> createKernel("kernels/particle.cl", "particle", program, kernel);

	// particle position input buffer
	particleBuffer = clCreateBuffer(cl -> getContext(), CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float) * numPoints * 8, particles, &error);
	cl -> checkError("clCreateBuffer(): particleBuffer", error);

	// define the arguments to our kernel
	clSetKernelArg(kernel, 0, sizeof(cl_mem), &particleBuffer);
	clSetKernelArg(kernel, 1, sizeof(int), &numPoints);
	clSetKernelArg(kernel, 4, sizeof(int), &numDivisions);

	// command queue for sending data to the kernel
	commandQueue = clCreateCommandQueue(cl -> getContext(),
										cl -> getDeviceID(0),
										0,
										&error);
	cl -> checkError("clCreateCommandQueue()", error);
}

void PointManager::update()
{
	const int NUM_DIVISION_ELEMENTS = numPoints / numDivisions;

	OpenCL *cl = OpenCL::getInstance();
	cl_int error;

	int start;
	int end;

	// compute which subgroup of particles all of our other particles will interact with
	divisionIndex ++;
	if(divisionIndex >= numDivisions)
		divisionIndex = 0;
	start = divisionIndex * NUM_DIVISION_ELEMENTS;
	end = start + NUM_DIVISION_ELEMENTS;

	// these correspond to the 3rd and 4th arguments to the particle() kernel function in particle.cl
	clSetKernelArg(kernel, 2, sizeof(int), &start);
	clSetKernelArg(kernel, 3, sizeof(int), &end);

	// send the work data to the kernel
	size_t workSize[1] = {(size_t)(numPoints)};						// in elements, not in bytes
	error = clEnqueueNDRangeKernel(commandQueue,					// our command queue
								   kernel,							// our kernel
								   1,								// dimensions of work
								   NULL,							// global work offset (docs specify that this must currently always be NULL)
								   workSize,						// global work size (array dimensions should match 3rd param)
								   NULL,							// local work size: NULL means OpenCL figures this out
								   0,								// no events in wait list
								   NULL,							// no event list to wait for before executing this command
								   NULL);							// no event is associated with this command
	cl -> checkError("clEnqueueNDRangeKernel()", error);
	clFinish(commandQueue);

	// read the completed data back into host memory
	error = clEnqueueReadBuffer(commandQueue,						// our command queue
								particleBuffer,						// CL buffer where we're getting the data from
								CL_TRUE,							// blocking read
								0,									// no offset to read from
								sizeof(float) * numPoints * 4,		// how much to read, in bytes
								particles,							// where to read to
								0,									// no event wait list
								NULL, 								// 0 events on wait list
								NULL);								// no event associated with this read command
	cl -> checkError("clEnqueueReadBuffer()", error);

	// send the resulting data back to the GPU for display with OpenGL
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * numPoints * 4, particles);				// only send the point positions, not the velocities
}

void PointManager::render(const mat4 &projection, const mat4 &view)
{
	shader -> bind();
	shader -> uniformMatrix4fv("u_Projection", 1, (GLfloat*)value_ptr(projection));
	shader -> uniformMatrix4fv("u_View", 1, (GLfloat*)value_ptr(view));

	glBindVertexArray(vao);
	glDrawArrays(GL_POINTS, 0, numPoints);
}
