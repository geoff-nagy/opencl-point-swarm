// OpenCL Point Swarm
// Geoff Nagy
// A quick program using OpenCL to hardware-accelerate a particle show.

#include "objects/pointmanager.h"					// point cloud handler

#include "util/gldebugging.h"						// for OpenGL callback debugging

#include "GL/glew.h"								// OpenGL extension wrangler, otherwise we only get GL 1.1 or so

#include "GLFW/glfw3.h"								// context, window, and input handling

#include "glm/glm.hpp"								// OpenGL Mathematics library to handle our vector and matrix math
#include "glm/gtc/matrix_transform.hpp"				// for lookAt() and perspective(), since we shouldn't use GLU for these (GLU is deprecated)
#include "glm/gtx/rotate_vector.hpp"				// used to compute camera viewing angles when rotating around the center of the world
using namespace glm;

#include <ctime>
#include <cstdlib>
#include <iostream>
using namespace std;

// GLFW window and characteristics
GLFWwindow *window;
vec2 windowSize;

// camera position
float cameraDistance, cameraAngleX, cameraAngleY;
vec3 cameraPos;

// track old mouse position for mouse control of camera
double oldMouseX, oldMouseY;

// properties of the perspective view that we use for rendering everything else
mat4 perspectiveProjection;
mat4 model;
mat4 perspectiveView;

// intialization functions
void openWindow();
void prepareOpenGL();
void prepareViewingParams();

// camera control
void mouseScrollCallback(GLFWwindow* window, double x, double y);
void moveCameraWithMouse();
void zoomCamera(double y);
void setCameraPosition(float distance, float angleX, float angleY);

int main(int args, char *argv[])
{
	const int NUM_POINTS = 1000000;

	// our objects to render
	PointManager *points;					// point renderer object

	// reseed random number generator
	srand(3);//time(NULL));		// 3 is nice, 4, 7, 10, 16, 22, 28, 29, 30

	// create our OpenGL window and context, and set some rendering options
	openWindow();
	prepareOpenGL();
	prepareViewingParams();

	// initialize our graphical objects
	points = new PointManager(NUM_POINTS);

	// we exit if the users presses the 'X' or the ESC key
	while(!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE))
	{
		// clear our colour buffer; there's no need to clear the depth buffer since we don't use it
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// update the camera position from mouse input
		cameraAngleY += 0.01;
		moveCameraWithMouse();
		setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);

		// control the points---this is done using OpenCL
		points -> update();

		// show our points
		points -> render(perspectiveProjection, perspectiveView);

		// get input events and update our framebuffer
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	// shut down our graphical objects and return the memory to the system
	delete points;

	// shut down GLFW
	glfwDestroyWindow(window);
	glfwTerminate();
}

void openWindow()
{
	const int WIDTH = 1280;
	const int HEIGHT = 720;
	const char *TITLE = "OpenCL Point Swarm Demo";

	int monitorWidth, monitorHeight;
	GLenum error;

	// we need to intialize GLFW before we create a GLFW window
	if(!glfwInit())
	{
		cerr << "openWindow() could not initialize GLFW" << endl;
		exit(1);
	}

	// explicitly set our OpenGL context to something that doesn't support any old-school shit
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_REFRESH_RATE, 60);
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_DECORATED, 1);
	glfwWindowHint(GLFW_FOCUSED, 1);
	#ifdef DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	#endif

	// use the current desktop mode to decide on a suitable resolution
	const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	monitorWidth = mode -> width;
	monitorHeight = mode -> height;

	// create our OpenGL window using GLFW
    window = glfwCreateWindow(WIDTH, HEIGHT,					// specify width and height
							  TITLE,							// title of window
							  glfwGetPrimaryMonitor(),								// always windowed mode
							  NULL);							// not sharing resources across monitors
	glfwMakeContextCurrent(window);
	glfwSetWindowPos(window,									// center the window
					(monitorWidth / 2.0) - (WIDTH / 2),
					(monitorHeight / 2.0) - (HEIGHT / 2));
	glfwSwapInterval(1);

	// disable the cursor
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// configure our viewing area
	glViewport(0, 0, WIDTH, HEIGHT);

	// enable our extensions handler
	glewExperimental = true;		// GLEW bug: glewInit() doesn't get all extensions, so we have it explicitly search for everything it can find
	error = glewInit();
	if(error != GLEW_OK)
	{
		cerr << glewGetErrorString(error) << endl;
		exit(1);
	}

	// clear the OpenGL error code that results from initializing GLEW
	glGetError();

	// use a background color we'll recognize
	glClearColor(0.0, 0.0, 0.0, 1.0);

	// save our window size for later on
	windowSize = vec2(WIDTH, HEIGHT);

	// print our OpenGL version info
    cout << "-- GL version:   " << (char*)glGetString(GL_VERSION) << endl;
    cout << "-- GL vendor:    " << (char*)glGetString(GL_VENDOR) << endl;
    cout << "-- GL renderer:  " << (char*)glGetString(GL_RENDERER) << endl;
	cout << "-- GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

	// OpenGL callback debugging is extremely useful, so enable it if we're in debug mode
	#ifdef DEBUG
		initGLDebugger();
		cout << "-- GL callback debugging is enabled" << endl;
	#else
		cout << "-- GL callback debugging is disabled in non-Debug configurations" << endl;
	#endif
}

void prepareOpenGL()
{
	// turn on depth testing and enable additive blending
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	// black screen in background
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void prepareViewingParams()
{
	const float FOV = 45.0f;
	const float ASPECT_RATIO = (float)windowSize.x / (float)windowSize.y;

	const float PERSPECTIVE_NEAR_RANGE = 1.0f;
	const float PERSPECTIVE_FAR_RANGE = 10000.0f;

	const float STARTING_CAMERA_DISTANCE = 250.0;
	const float STARTING_CAMERA_ANGLE_X = -M_PI / 4.0;
	const float STARTING_CAMERA_ANGLE_Y = 0.0f;

	// compute our perspective projection---this never changes so we can just do it once
	perspectiveProjection = perspective(FOV, ASPECT_RATIO, PERSPECTIVE_NEAR_RANGE, PERSPECTIVE_FAR_RANGE);

	// set up our mouse
	glfwSetScrollCallback(window, mouseScrollCallback);

	// position our camera somewhere sane to start our with
	cameraDistance = STARTING_CAMERA_DISTANCE;
	cameraAngleX = STARTING_CAMERA_ANGLE_X;
	cameraAngleY = STARTING_CAMERA_ANGLE_Y;
	setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);
}

void mouseScrollCallback(GLFWwindow* window, double x, double y)
{
	const float SCROLL_SPEED = 4.0;				// meters we scroll per scroll notch
	const float MIN_DIST = 4.0;					// how close we're allowed to get
	const float MAX_DIST = 5000.0;				// how far we're allowed to get

	// adjust the camera distance either forwards or backwards, and keep it within a reasonable distance
	cameraDistance -= (y > 0.0 ? SCROLL_SPEED : -SCROLL_SPEED);
	if(cameraDistance > MAX_DIST) cameraDistance = MAX_DIST;
	if(cameraDistance < MIN_DIST) cameraDistance = MIN_DIST;
}

void moveCameraWithMouse()
{
	const float MAX_ANGLE = (M_PI / 2.0f) - 0.001;
	double mouseX, mouseY;

	// always update our mouse position
	glfwGetCursorPos(window, &mouseX, &mouseY);

	// only do something if we detect a mouse button press
	if(glfwGetMouseButton(window, 0))
	{
		cameraAngleX -= (mouseY - oldMouseY) * 0.01;
		cameraAngleY -= (mouseX - oldMouseX) * 0.01;
		if(cameraAngleX > MAX_ANGLE) cameraAngleX = MAX_ANGLE;
		if(cameraAngleX < -MAX_ANGLE) cameraAngleX = -MAX_ANGLE;
	}

	// track our old mouse position for mouse movement calculations next frame
	oldMouseX = mouseX;
	oldMouseY = mouseY;
}

void setCameraPosition(float distance, float angleX, float angleY)
{
	const vec3 CAMERA_LOOK(0.0, 0.0, 0.0);		// look at center position
	const vec3 CAMERA_UP(0.0, 1.0, 0.0);		// define what direction the camera considers to be 'up'

	// position our camera around the center point based on the incoming distance and viewing angle
	cameraPos = vec3(0.0, 0.0, cameraDistance);
	cameraPos = rotate(cameraPos, angleX, vec3(1.0, 0.0, 0.0));
	cameraPos = rotate(cameraPos, angleY, vec3(0.0, 1.0, 0.0));

	// compute a matrix describing the position and orientation of our camera
	perspectiveView = lookAt(cameraPos, CAMERA_LOOK, CAMERA_UP);
}
