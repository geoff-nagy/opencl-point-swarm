# OpenCL Particle Swarm

This is a simple OpenCL/OpenGL program that demonstrates 1 million self-interacting particles on a GPU. Built for Windows only, but there's no reason it couldn't be compiled under Linux or another operating system. You will need to make sure you have the appropriate OpenCL support.

## Running

Both the debug and release executables are included in the root directory. Use the the mouse (click-and-drag) to rotate, and the mouse wheel to zoom.

## Compiling

Executable is already included, but if you want to make modifications and build it, you'll need Code::Blocks. If you don't have it already, you can download Code::Blocks from [here](http://codeblocks.org/downloads/26).

## License

Demo code is in the public domain. Do whatever you wish with it.

## Contact

Feel free to contact Geoff at geoff.nagy@gmail.com if you have any questions.